# Stackmesh Core

Stackmesh Core provides the canonical implementation of the Stackmesh protocols and offers a simple core library
that may easily be used on various devices/platforms with only a few simple integrations.

Note: This repository serves as an upstream dependency for Stackmesh Server and Stackmesh Mobile.
