package filesystem

import (
  "github.com/hanwen/go-fuse/fuse"
	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse/pathfs"

  "gitlab.com/stackmesh/core/store"
)


type FS struct{
  pathfs.FileSystem
  initAt time.Time
}

func New(store *store.Store) (*FS, error) {
  return &FS{
    FileSystem: pathfs.NewDefaultFileSystem(),
    initAt: time.Now().UTC(),
  }, nil
}

type Opts struct{
  Debug bool
}

var DefaultOpts = Opts{}

func (f *FS) Mount(path string, opts *Opts) error {
  server, conn, err := nodefs.MountRoot(path, pathfs.NewPathNodeFs(f, nil).Root(), nil)
  if err != nil {
    return err
  }

  server.SetDebug(opts.Debug)
  server.Serve()

  return nil
}

func (f *FS) Unmount() {}

func (f *FS) GetAttr(name string, context *fuse.Context) (*fuse.Attr, fuse.Status) {
  return nil, fuse.ENOENT
}

func (f *FS) OpenDir(name string, context *fuse.Context) ([]fuse.DirEntry, fuse.Status) {
  return nil, fuse.ENOENT
}

func (f *FS) Open(name string, flags uint32, context *fuse.Context) (nodefs.File, fuse.Status) {
  return nil, fuse.ENOENT
}

func (f *FS) Create(name string, flags uint32, mode uint32, context *fuse.Context) (nodefs.File, fuse.Status) {
  nil, fuse.EROFS
}

func (f *FS) Mkdir(name string, mode uint32, context *fuse.Context) (*nodefs.Inode, fuse.Status) {
  return nil, fuse.OK
}

func (f *FS) Rmdir(name string, context *fuse.Context) fuse.Status {
  return fuse.OK
}
