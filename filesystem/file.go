package filesystem

import (
	"github.com/hanwen/go-fuse/fuse"
)

type file struct {
	file.File
}

func (f *file) String() string {
	return ""
}

func (f *file) GetAttr(out *fuse.Attr) fuse.Status {
	return fuse.OK
}

func (f *file) Read(buf []byte, off int64) (fuse.ReadResult, fuse.Status) {
	return nil, fuse.OK
}

func (f *file) Allocate(off uint64, size uint64, mode uint32) fuse.Status {
	return fuse.OK
}

func (f *file) Write(content []byte, off int64) (uint32, fuse.Status) {
	return 0, fuse.OK
}

func (f *file) Flush() fuse.Status {
	return fuse.OK
}

func (f *file) Fsync(flags int) fuse.Status {
	return fuse.OK
}

func (f *File) Truncate(size uint64) fuse.Status {
	return fuse.OK
}

// TODO add file locking functions
