package service

import (
	"fmt"
	"log"
	"net"
	"time"

	"gitlab.com/stackmesh/core/store"
)

type SGPService struct {
	sstore  *store.Store
	killSig chan bool
}

func New(s *store.Store) *SGPService {
	return &SGPService{
		sstore:  s,
		killSig: make(chan bool),
	}
}

func (s *SGPService) Run() error {
	addr, err := net.ResolveUDPAddr("udp4", "0.0.0.0:5000")
	if err != nil {
		return err
	}

	conn, err := net.ListenUDP("udp4", addr)
	if err != nil {
		return err
	}

	for {
		buff := make([]byte, 1500)
		conn.SetReadDeadline(time.Now().Add(time.Millisecond * 500))

		n, raddr, err := conn.ReadFromUDP(buff)
		if err != nil {
			log.Println("sgp read error: " + err.Error())
		} else {
			buff = buff[:n]
			fmt.Println(string(buff), raddr.String())

			// TODO get mac address/id from store
			baddr, _ := net.ResolveUDPAddr("udp4", "255.255.255.255:5000")
			b2addr, _ := net.ResolveUDPAddr("udp4", "255.255.255.0:5000")

			// TODO handle errors
			conn.SetWriteDeadline(time.Now().Add(time.Millisecond * 500))
			conn.WriteToUDP([]byte("ping from tuxbook"), baddr)
			conn.WriteToUDP([]byte("ping from tuxbook"), b2addr)

			select {
			case <-s.killSig:
				fmt.Println("exiting sgp net loop")
				break
			}
		}
	}

	return nil
}

func (s *SGPService) Close() {
	s.killSig <- true
}
