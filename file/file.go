package file

import (
	"bytes"
	"errors"
	"io"
	"time"

	"github.com/vmihailenco/msgpack"
)

type FileType uint8

const (
	REG FileType = 0
	DIR FileType = 1
)

type Header struct {
	CreateAt time.Time
	ModAt    time.Time
	AccessAt time.Time

	Type  FileType
	Size  uint64
	Name  string
	Owner uint64
}

func (h Header) Bytes() ([]byte, error) {
	return msgpack.Marshal(h)
}

func HeaderFromBytes(dat []byte) (*Header, error) {
	header := Header{}
	err := msgpack.Unmarshal(dat, &header)
	if err != nil {
		return nil, err
	}

	return &header, nil
}

type File interface {
	Header() Header

	// Files will return a list of ids
	// if file type is dir or an error
	// and the filename if reg
	Files() ([]string, error)

	// Stream will return nil if dir
	Stream() io.ReadWriter
}

type DatFile struct {
	IHeader Header
	Body    []byte
}

func (d *DatFile) Header() Header {
	return d.IHeader
}

func (d *DatFile) Files() ([]string, error) {
	if d.IHeader.Type == REG {
		return []string{d.IHeader.Name}, errors.New("file is a directory")
	}

	return []string{}, nil

	// TODO deserialize body if DIR and return
}

func (d *DatFile) Stream() io.ReadWriter {
	return bytes.NewBuffer(d.Body)
}
