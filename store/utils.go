package store

import (
	"errors"
	"fmt"
	"strings"
)

// parsePath cleans and parses the path string
// returning an array of ordered path segments
func parsePath(in string) ([]string, error) {
	// check if base path doesn't start with /
	if string(in[0]) != "/" {
		return nil, errors.New(fmt.Sprintf("invalid base path: '%s', all paths must start with '/'", in))
	}

	// trim the starting /
	in = in[1:]

	// trim following / if exists
	if string(in[len(in)-1]) == "/" {
		in = in[:len(in)-1]
	}

	return strings.Split(in, "/"), nil
}

func toKey(in []string) string {
	out := ""
	for _, val := range in {
		out += fmt.Sprintf("%s#", val)
	}

	return out
}
