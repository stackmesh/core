package store

import (
	uuid "github.com/satori/go.uuid"
)

type Observer struct {
}

func (o *Observer) Watch(callback func(string, []EventType), files ...string) WatcherId {
	return WatcherId(uuid.NewV4().Bytes())
}

func (o *Observer) Unwatch(id WatcherId) {

}

type EventType uint

const (
	CREATE EventType = 0
	READ   EventType = 1
	UPDATE EventType = 2
	DELETE EventType = 3

	LOCK  EventType = 4
	ULOCK EventType = 5

	SUB  EventType = 6
	USUB EventType = 7
)

type WatcherId string

type CbGrouper struct {
	callbacks []string
	recursive bool
}
