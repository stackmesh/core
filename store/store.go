package store

import (
	"github.com/dgraph-io/badger"
)

// Opts is a object used to configure
// new Store instances.
type Opts struct {
	// DiskDir is the directory in which to
	// store disk-persisted files/data.
	DiskDir string
}

// Store is protected datastore that provides
// methods for tracking changes in realtime,
// using memory-only or disk-persisted files.
// Currently no methods are provided for index
// -based searching such as a reverse index, b
// -tree etc. For those wishing to provide search
// functions, creating a root level watcher is
// recommended as it would be extremely simple
// maintain an index using the information
// provided within. Simply layer your own search
// implementation over the provided transaction
// api (all methods attached to the Transaction
// Type) and voila!
type Store struct {
	patchDict map[string]string
	mem       map[string][]byte
	disk      *badger.DB
	observer  *Observer
}

// New creates a new store instance, checking for
// a pre-existing patch dictionary.
func New(opts *Opts) (*Store, error) {
	dopts := badger.DefaultOptions
	dopts.Dir = opts.DiskDir
	dopts.ValueDir = opts.DiskDir

	db, err := badger.Open(dopts)
	if err != nil {
		return nil, err
	}

	return &Store{
		disk: db,
		mem:  make(map[string][]byte),
		patchDict: map[string]string{
			// virtual, system and their
			// shorthand aliases are used
			// to create a memory-only
			// namespace for storing files.
			"vrt":     "vrt",
			"virtual": "vrt",
			"sys":     "sys",
			"system":  "sys",
		},
	}, nil
}

// Close waits for pending operations and
// closes all ties to the disk persistance.
// Close may also back up the patch dictionary
// if it is not in sync.
func (s *Store) Close() error {
	return s.disk.Close()
}

// Observer returns a new store observer
// which can be used to watch files in
// real-time. Observers are used to track
// what files are being watched by who, as
// well as provide optional authorization
// for files with ACLs
func (s *Store) Observer() *Observer {
	return s.observer
}

// Tx returns a new store Transaction
// which should be used to read and write
// files safely. Transactions can be
// batched for shorter trips, and also
// provide mechanisms for authorization
// for files with a ACL
func (s *Store) Tx() *Transaction {
	return &Transaction{
		store: s,
	}
}

// BatchTx is a transaction that provides
// methods for manually commiting updates
// in bulk. This is highly recommended for
// any case where more than one file needs
// to be retrieved in a single round.
func (s *Store) BatchTx() *Transaction {
	return &Transaction{
		batch: true,
		store: s,
	}
}

func (s *Store) resolvePatches(in []string) []string {
	out := []string{}

	for _, v := range in {
		val, ok := s.patchDict[v]
		if ok {
			out = append(out, val)
		} else {
			out = append(out, v)
		}
	}

	return out
}
