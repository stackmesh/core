package store

import (
	"errors"
	"io/ioutil"

	"github.com/dgraph-io/badger"
	"gitlab.com/stackmesh/core/file"
)

type Transaction struct {
	store *Store
	batch bool
}

func (t *Transaction) Exec() error {
	if !t.batch {
		return errors.New("cannot execute non-batch transaction")
	}

	return nil
}

func (t *Transaction) Query(filepath string) (file.File, error) {
	path, err := parsePath(filepath)
	if err != nil {
		return nil, err
	}

	patchedPath := t.store.resolvePatches(path)
	switch patchedPath[0] {
	// vrt paths are in the memory-only storage
	case "vrt":
		headerBytes, ok := t.store.mem[toKey(patchedPath)+"^_h_"]
		if !ok {
			return nil, errors.New("file not found")
		}

		header, err := file.HeaderFromBytes(headerBytes)
		if err != nil {
			return nil, errors.New("file header corrupt: " + err.Error())
		}

		body, ok := t.store.mem[toKey(patchedPath)+"^_b_"]
		if !ok {
			return nil, errors.New("file body missing or corrupt")
		}

		return &file.DatFile{
			IHeader: *header,
			Body:    body,
		}, nil

		// vrt paths are in the device/system abstractions
	case "sys":
		headerBytes, ok := t.store.mem[toKey(patchedPath)+"^_h_"]
		if !ok {
			return nil, errors.New("file not found")
		}

		header, err := file.HeaderFromBytes(headerBytes)
		if err != nil {
			return nil, errors.New("file header corrupt: " + err.Error())
		}

		body, ok := t.store.mem[toKey(patchedPath)+"^_b_"]
		if !ok {
			return nil, errors.New("file body missing or corrupt")
		}

		return &file.DatFile{
			IHeader: *header,
			Body:    body,
		}, nil

	// all other paths are disk-backed
	default:
		var headerBytes, body []byte
		txn := t.store.disk.NewTransaction(false)
		headerItem, err := txn.Get([]byte(toKey(patchedPath) + "^_h_"))
		if err != nil {
			return nil, errors.New("file not found: " + err.Error())
		}

		bodyItem, err := txn.Get([]byte(toKey(patchedPath) + "^_b_"))
		if err != nil {
			return nil, errors.New("file body missing or corrupt: " + err.Error())
		}

		err = txn.Commit(nil)
		if err != nil {
			return nil, errors.New("file body missing or corrupt: " + err.Error())
		}

		headerBytes, _ = headerItem.Value()
		body, _ = bodyItem.Value()

		header, err := file.HeaderFromBytes(headerBytes)
		if err != nil {
			return nil, errors.New("file header corrupt")
		}

		return &file.DatFile{
			IHeader: *header,
			Body:    body,
		}, nil
	}

	return nil, nil
}

func (t *Transaction) Commit(filepath string, data file.File) error {
	path, err := parsePath(filepath)
	if err != nil {
		return err
	}

	patchedPath := t.store.resolvePatches(path)
	switch patchedPath[0] {
	// vrt paths are in the memory-only storage
	case "vrt":
		break
	case "sys":
		break
	default:
		t.store.disk.Update(func(txn *badger.Txn) error {
			// if file is nil, delete the file
			if data == nil {
				err := txn.Delete([]byte("key"))
				if err != nil {
					return err
				}

				return txn.Delete([]byte("key"))
			}

			headerDat, err := data.Header().Bytes()
			if err != nil {
				return err
			}

			err = txn.Set([]byte("key"), headerDat)
			if err != nil {
				return err
			}

			body, err := ioutil.ReadAll(data.Stream())
			if err != nil {
				return err
			}

			err = txn.Set([]byte("key"), body)
			if err != nil {
				return err
			}

			return nil
		})
		break
	}

	return nil
}
